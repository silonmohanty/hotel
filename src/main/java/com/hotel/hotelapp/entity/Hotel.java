package com.hotel.hotelapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table

public class Hotel  implements Serializable {
	
	
	
	@Id
	@GenericGenerator(name="m_auto",strategy="increment")
	@GeneratedValue(generator="m_auto")
	
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private  String type;
	
	@Column(name="averageprice")
	private  Double averagePrice ;
	
	
	@Column(name="rating")
	private   int rating;
	
	
	@Column(name="city")
	private String city;
	
	@Column(name="contactNumber")
	private  Long contactNumber;
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAveragePrice() {
		return averagePrice;
	}
	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}
	public int getRatting() {
		return rating;
	}
	public void setRatting(int ratting) {
		this.rating = ratting;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", type=" + type + ", averagePrice=" + averagePrice + ", rating="
				+ rating + ", city=" + city + ", contactNumber=" + contactNumber + "]";
	}
	
	

}
